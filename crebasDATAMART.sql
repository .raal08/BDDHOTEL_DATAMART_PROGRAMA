
/*==============================================================*/
/* Table: DIM_FACTURA                                           */
/*==============================================================*/
create table DIM_FACTURA
(
   FACTURA_ID           int not null,
   NOMBRE               varchar(100),
   FACTURA_FECHA        date,
   FACTURA_FECHAFIN     date,
   primary key (FACTURA_ID)
);

/*==============================================================*/
/* Table: DIM_HABITACION                                        */
/*==============================================================*/
create table DIM_HABITACION
(
   HABITACION_ID        int not null,
   HABITACION_NUMERO    int,
   HT_DESCRIPCION        longtext,
   primary key (HABITACION_ID)
);


/*==============================================================*/
/* Table: HC_HUESPED                                            */
/*==============================================================*/
create table HC_HUESPED
(
   FACTURA_ID           int not null,
   HABITACION_ID        int not null,
   TOTAL                decimal,
   primary key (FACTURA_ID, HABITACION_ID)
);


alter table HC_HUESPED add constraint FK_DIM_FACTURA_HC_HUESPED foreign key (FACTURA_ID)
      references DIM_FACTURA (FACTURA_ID) on delete restrict on update restrict;

alter table HC_HUESPED add constraint FK_DIM_HABITACION_HC_HUESPED foreign key (HABITACION_ID)
      references DIM_HABITACION (HABITACION_ID) on delete restrict on update restrict;


