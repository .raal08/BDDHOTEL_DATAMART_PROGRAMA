


 -------EXTRAER LOS DATOS DE HABITACION---------
  
select
habitacion.HABITACION_ID,HABITACION.HABITACION_NUMERO , habitacion_tipo.HT_DESCRIPCION
FROM habitacion  inner join habitacion_tipo ON (habitacion.HT_ID=habitacion_tipo.HT_ID)

--------EXTRAER LOS DATOS DE FACTURA-------------
select
FACTURA.FACTURA_ID,CLIENTE.CLIENTE_NOMBRE , FACTURA.FACTURA_FECHA,FACTURA.FACTURA_FECHAFIN
FROM FACTURA  inner join CLIENTE ON (FACTURA.CLIENTE_ID=CLIENTE.CLIENTE_ID)

-------------EXTRAER LOS DATOS PARA FORMAR LA TABLA HECHOS-------
   select 
   habitacion.HABITACION_ID,
   factura.FACTURA_ID,
   factura.FACTURA_FECHA
   FROM factura
   INNER JOIN HABITACION on FACTURA.HABITACION_ID=HABITACION.HABITACION_ID

------------------------
select 
CLIENTE.CLIENTE_NOMBRE AS NOMBRE,
HABITACION_TIPO.HT_DESCRIPCION AS DESCRIPCION_DE_HABITACION,
HABITACION.HABITACION_NUMERO as NÚMERO_DE_HABITACION, 
count(FACTURA.FACTURA_ID) as NÚMERO_DE_ALOJAMIENTO,
FACTURA.FACTURA_FECHA AS FECHA_ENTRADA,
FACTURA.FACTURA_FECHAFIN AS FECHA_SALIDA
from HABITACION
	inner join FACTURA on FACTURA.HABITACION_ID = HABITACION.HABITACION_ID
    inner join HABITACION_TIPO on HABITACION_TIPO.HT_ID = HABITACION.HT_ID
     inner JOIN CLIENTE ON CLIENTE.cliente_ID=FACTURA.cliente_ID
	group by NÚMERO_DE_HABITACION
    order by NÚMERO_DE_HABITACION asc;
	
	
	
select 
dim_habitacion.HABITACION_NUMERO,
dim_factura.CLIENTE_NOMBRE,
dim_habitacion.HT_DESCRIPCION,
dim_factura.FACTURA_FECHA,
dim_factura.FACTURA_FECHAFIN 
from hc_huesped 
inner join  dim_factura on hc_huesped.factura_id = dim_factura.factura_id
inner join  dim_habitacion on hc_huesped.HABITACION_ID = dim_habitacion.HABITACION_ID




-----------------CONSULTA PARA PODER CONECTARSE AL SPOON DATA_INTEGRATION-----------
alter user 'root'@'localhost' identified with mysql_native_password by '654321';

flush privileges;